#ifndef _commontools_h_
#define _commontools_h_

#include "TString.h"
#include "TFile.h"
#include "TChain.h"

#include <iostream>
#include <string>

//
//converts an eta-phi square to a cartesian trapezoid and returns its area
//
float getSciArea(float z, float eta, float phi,float sciSide) {
  float thetaUp( 2*atan(exp(-(eta-sciSide))) ), thetaDn( 2*atan(exp(-(eta+sciSide))) );
  float yUp( z*tan(thetaUp) ), yDn( z*tan(thetaDn) );
  float xLfUp( yUp/tan(phi-sciSide) ), xLfDn( yDn/tan(phi-sciSide) );
  float xRtUp( yUp/tan(phi+sciSide) ), xRtDn( yDn/tan(phi+sciSide) );
  float h=fabs(yUp-yDn);
  float a=fabs(xLfUp-xLfDn);
  float b=fabs(xRtUp-xRtDn);
  return 0.5*(a+b)*h;
}

//
//OPENS FILES AND CHECKS HOW MANY EVENTS ARE AVAILABLE
//
Int_t checkFileIntegrity(TString url,TString tname)
{
  Int_t nentries(0);
  TFile *fIn=TFile::Open(url);
  if(fIn)
    {
      if(!fIn->IsZombie()) 
        {
          TTree *t=(TTree *)fIn->Get(tname);
          if(t) nentries=t->GetEntriesFast();
        }      
      fIn->Close();
    }
  return nentries;
}

//
// INPUT: CHECK THAT FOR EACH DIGI THERE IS A CORRESPONDING SIM FILE 
// AND MAKE THE TWO CHAINS FRIENDS SO THEY CAN BE READ SIMULTANEOUSLY
//
std::pair<TChain *,TChain *>getEventChain(std::string recoTreeName,
                                          std::string recoTag,
                                          std::string simTreeName,
                                          std::string simTag,
                                          std::string inDir,
                                          unsigned pStartInputs,
                                          unsigned pNinputs,
                                          unsigned debug,
                                          bool onlySim=false)
{
  TChain *reco = new TChain(recoTreeName.c_str());
  TChain *sim = new TChain(simTreeName.c_str());
  TSystemDirectory dir(inDir.c_str(),inDir.c_str());
  TList *files = dir.GetListOfFiles();
  TSystemFile *file;
  TIter next(files);
  unsigned ngood(0);
  unsigned ninputs(0);
  while ((file=(TSystemFile*)next())) {
    if( file->IsDirectory() ) continue;
    TString simfname( file->GetName() );
    if( !simfname.Contains(".root") ) continue;
    if( !simfname.Contains(simTag.c_str()) ) continue;
    Int_t nsim=checkFileIntegrity(inDir+"/"+simfname,simTreeName);
    TString recfname(simfname);
    recfname=recfname.ReplaceAll(simTag,recoTag);
    Int_t nreco=checkFileIntegrity(inDir+"/"+recfname,recoTreeName);
    if(onlySim)
      {
        if(ngood>=pStartInputs)
          {
            sim->AddFile(inDir+"/"+simfname);
            ninputs+=1;
          }
        ngood+=1;
      }
    else if(nreco>0 && nsim==nreco)
      {
        if(ngood>=pStartInputs)
          {
            reco->AddFile(inDir+"/"+recfname);        
            sim->AddFile(inDir+"/"+simfname);
            ninputs+=1;
          }
        ngood+=1;
      }
    else
      {
        std::cout << "[Warn] will discard inputs in " << recfname << "/" << simfname << std::endl;
      }
    if(debug>0)
      {
        std::cout << recfname << " " << simfname << std::endl
                  << "\t" << nreco << " reco / " << nsim  << " sim" << std::endl;
      }
    
    //check if enough files were collected already
    if(pNinputs>0 && ninputs>= pNinputs) break;
  }
  if(!onlySim)
    {
      reco->AddFriend(sim);
      std::cout << "Total events available:" << reco->GetEntries() << std::endl;
    }
  else
    {
      std::cout << "Total events available:" << sim->GetEntries() << std::endl;
    }

  std::pair<TChain *,TChain *> evChain(reco,sim);
  return evChain;
}

//uses first file to read HGCSSInfo
HGCSSDetector &getDetector(TChain *sim) 
{
  TFile *curF=sim->GetFile();
  HGCSSInfo *info=(HGCSSInfo*)curF->Get("Info");
  std::cout << curF->GetName() << std::endl;
  assert(info);
  unsigned versionNumber = info->version();
  unsigned model         = info->model();
  std::cout << "Starting detector v" << versionNumber << " model" << model << std::endl;
  HGCSSDetector &myDetector=theDetector();
  myDetector.buildDetector(info->version());
  return myDetector;
}

#endif
