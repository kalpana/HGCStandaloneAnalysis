# name of the library
LIBNAME = HGCStandaloneAnalysis

#Necessary to use shell built-in commands
SHELL=bash

# Define include paths
USERINCLUDES += `root-config --cflags --libs`
USERINCLUDES += -Iinclude/ 
USERINCLUDES += -I$(BASEINSTALL)/include/
USERINCLUDES += -I../PFCalEE/userlib/include/ -I../PFCalEE/analysis/include

# Define libraries to link
USERLIBS += $(shell root-config --glibs) -lGenVector # -lTreePlayer -lTMVA 
USERLIBS += -Wl,-rpath, -L$(BASEINSTALL)/lib -lfastjettools -lfastjet -lfastjetplugins -lsiscone_spherical -lsiscone -lgfortran
USERLIBS += -lboost_regex -lboost_program_options -lboost_filesystem
USERLIBS += -L../PFCalEE/userlib/lib -lPFCalEEuserlib

CXXFLAGS = -Wall -W -O2 # -std=c++11 
LDFLAGS = -shared -Wall -W
CXX=g++
LD=g++

CXXFLAGS += $(USERINCLUDES)
LIBS += $(USERLIBS)

# A list of directories
BASEDIR = $(shell pwd)
LIBDIR = $(BASEDIR)/lib
EXEDIR = $(BASEDIR)/bin
SRCDIR = $(BASEDIR)/src
DOCDIR= $(BASEDIR)/docs
OBJ_EXT=o
TEST_EXT=cpp

# Build a list of srcs and bins to build
SRCS=$(wildcard $(BASEDIR)/src/*.cc)
EXES=$(wildcard $(BASEDIR)/test/*.cpp)
OBJS=$(subst $(SRCDIR), $(OBJDIR),$(subst cc,$(OBJ_EXT),$(SRCS)))

BINS=$(EXEDIR)/prepareCalibrationTree

SUBDIRS = userlib

.PHONY: $(SUBDIRS) all
all: $(BINS)

docs: all
	doxygen Doxyfile

$(SUBDIRS):
	$(MAKE) -C $@

$(EXEDIR)/prepareCalibrationTree:  $(SRCDIR)/prepareCalibrationTree.cpp $(LIBDIR)/lib$(LIBNAME).so $(wildcard $(BASEDIR)/include/*.h*)
	$(CXX) -o $@ $(CXXFLAGS) $< $(LIBS) -L$(LIBDIR) -l$(LIBNAME)

$(LIBDIR)/lib$(LIBNAME).so:  $(OBJS) $(LIBDEPENDS)
	$(LD) $(LDFLAGS) -o $(LIBDIR)/lib$(LIBNAME).so $(OBJS) $(LIBS)

lib: $(LIBDIR)/lib$(LIBNAME).so

dict:
	rootcint -v -f src/dict.cc -c include/LinkDef.h
	mv src/dict.h include/dict.h

clean: 
	rm -rf $(OBJS)  $(LIBDIR)/lib$(LIBNAME).so $(BINS) 
