#include<string>
#include<iostream>
#include<fstream>
#include<sstream>
#include<map>
#include <boost/algorithm/string.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include "boost/function.hpp"

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH3F.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TF1.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TRandom3.h"
#include "TMatrixD.h"
#include "TMatrixDSym.h"
#include "TVectorD.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TVector2.h"
#include "TRandom2.h"
#include "TSystem.h"

#include "HGCSSEvent.hh"
#include "HGCSSInfo.hh"
#include "HGCSSSamplingSection.hh"
#include "HGCSSSimHit.hh"
#include "HGCSSRecoHit.hh"
#include "HGCSSGenParticle.hh"
#include "HGCSSParameters.hh"
#include "HGCSSCalibration.hh"
#include "HGCSSDigitisation.hh"
#include "HGCSSDetector.hh"
#include "HGCSSGeometryConversion.hh"
#include "HGCSSPUenergy.hh"
#include "HGCSSMipHit.hh"

#include "PositionFit.hh"
#include "SignalRegion.hh"

#include "Math/Vector3D.h"
#include "Math/Vector3Dfwd.h"
#include "Math/Point2D.h"
#include "Math/Point2Dfwd.h"

#include "utilities.h"

#include "fastjet/ClusterSequence.hh"

#include "CommonTools.h"

#define MAXHITS 50000

using boost::lexical_cast;
namespace po=boost::program_options;
using namespace std;
using namespace fastjet;

int main(int argc, char** argv){//main  

  TRandom2 rand;

  //
  // CONFIGURATION
  //

  //configuration parameters
  unsigned pNevts,pStartInputs,pNinputs,pNPhysLayers;
  std::string inDir,outFile,recoTag,simTag,recoTreeName,simTreeName;
  unsigned debug;
  float enmin,dRwindow,icsmear;

  //read configuration from file
  std::string cfg;
  po::options_description preconfig("Configuration"); 
  preconfig.add_options()("cfg,c",po::value<std::string>(&cfg)->required());
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(preconfig).allow_unregistered().run(), vm);
  po::notify(vm);
  po::options_description config("Configuration");
  config.add_options()
    //Input output and config options //->required()
    ("pNevts",       po::value<unsigned>(&pNevts)->default_value(0))
    ("pStartInputs", po::value<unsigned>(&pStartInputs)->default_value(0))
    ("pNinputs",     po::value<unsigned>(&pNinputs)->default_value(1))
    ("inDir",        po::value<std::string>(&inDir)->required())
    ("outFile",      po::value<std::string>(&outFile)->required())
    ("debug",        po::value<unsigned>(&debug)->default_value(0))
    ("recoTag",      po::value<std::string>(&recoTag)->required())
    ("simTag",       po::value<std::string>(&simTag)->required())
    ("recoTreeName", po::value<std::string>(&recoTreeName)->required())
    ("simTreeName",  po::value<std::string>(&simTreeName)->required())
    ("enmin",        po::value<float>(&enmin)->default_value(1.0))
    ("icsmear",      po::value<float>(&icsmear)->default_value(-1))
    ("dRwindow",     po::value<float>(&dRwindow)->default_value(1.0))
    ("pNPhysLayers", po::value<unsigned>(&pNPhysLayers)->default_value(50))
    ;
  po::store(po::command_line_parser(argc, argv).options(config).allow_unregistered().run(), vm);
  po::store(po::parse_config_file<char>(cfg.c_str(), config), vm);
  po::notify(vm);

  std::cout << " -- Input parameters: " << std::endl
	    << " -- Input directory: " << inDir << std::endl
	    << " -- Output file path: " << outFile << std::endl
	    << " -- Processing ";
  if (pNevts == 0) std::cout << "all events." << std::endl;
  else             std::cout << pNevts << " events per run." << std::endl;
  if(pStartInputs>0) std::cout << "Starting at input file #" << pStartInputs << std::endl;
  if(pNinputs>0)     std::cout << "Processing " << pNinputs << " files" << std::endl;
  std::cout << "Reco tag: " << recoTag << " sim tag:" << simTag << std::endl;

  std::pair<TChain *,TChain *> evChain=getEventChain(recoTreeName,recoTag,simTreeName,simTag,inDir,pStartInputs,pNinputs,debug);
  TChain *reco=evChain.first;
  TChain *sim=evChain.second;
  
  HGCSSDetector &myDetector = getDetector(sim);
  Int_t nLayers=myDetector.nLayers();
  Int_t nVirtualLayers=nLayers-pNPhysLayers;

  TFile *curF=sim->GetFile();
  HGCSSInfo *info=(HGCSSInfo*)curF->Get("Info");
  float siArea(1.5*sqrt(3.)*pow(info->cellSize(),2));
  float sci1Side(TMath::Pi()*2./360.),sci2Side(TMath::Pi()*2./288.);
  
  const unsigned model = info->model();
  const double cellSize = info->cellSize();
  HGCSSGeometryConversion geomConv(model,cellSize,false,3);

  //decode the tree
  HGCSSEvent * event = 0;
  std::vector<HGCSSRecoHit> * rechitvec = 0;
  std::vector<HGCSSSimHit> *simhitvec=0;
  std::vector<HGCSSGenParticle> *genpartvec=0;
  std::vector<HGCSSSamplingSection> *samplesecvec=0;
  unsigned nPuVtx = 0;
  reco->SetBranchAddress("HGCSSEvent",&event);
  reco->SetBranchAddress("HGCSSRecoHitVec",&rechitvec);
  reco->SetBranchAddress("HGCSSSimHitVec",&simhitvec);
  reco->SetBranchAddress("HGCSSGenParticleVec",&genpartvec);
  reco->SetBranchAddress("HGCSSSamplingSectionVec",&samplesecvec);
  if (reco->GetBranch("nPuVtx")) reco->SetBranchAddress("nPuVtx",&nPuVtx);

  //
  // PREPARE OUTPUT
  //
  TString baseOutputName(gSystem->BaseName(outFile.c_str()));
  TFile *outputFile = TFile::Open(baseOutputName,"RECREATE");
  if (!outputFile) {
    std::cout << " -- Error, output file " << baseOutputName << " cannot be opened. Please create output directory. Exiting..." << std::endl;
    return 1;
  }
  else {
    std::cout << " -- output file " << outputFile->GetName() << " successfully opened." << std::endl;
  }
  outputFile->cd();  

  TTree *hitTree=new TTree("hits","hits"); 
  Int_t genId;
  Int_t nHadrons[pNPhysLayers],nGammas[pNPhysLayers],nMuons[pNPhysLayers],nElectrons[pNPhysLayers];
  Float_t genEn,genEt,genEta,genPhi;
  Int_t nhits, Event=0;
  Bool_t  hit_si[MAXHITS];
  Int_t   hit_lay[MAXHITS],hit_sithick[MAXHITS];
  Float_t hit_x[MAXHITS],hit_y[MAXHITS],hit_z[MAXHITS],hit_men[MAXHITS],hit_en[MAXHITS],hit_endens[MAXHITS],hit_dR[MAXHITS],hit_dRho[MAXHITS];
  Float_t si_sumen[pNPhysLayers],sci_sumen[pNPhysLayers],si_clustsumen[5][pNPhysLayers],sci_clustsumen[5][pNPhysLayers],hit_miss[pNPhysLayers];
  Float_t total_sim_men[pNPhysLayers],avg_emFrac[pNPhysLayers],avg_hadFrac[pNPhysLayers];
  hitTree->Branch("Event", Event,"Event/I");
  hitTree->Branch("nHadrons",    nHadrons,   Form("nHadrons[%d]/I",pNPhysLayers) );
  hitTree->Branch("nGammas",     nGammas,    Form("nGammas[%d]/I",pNPhysLayers) );
  hitTree->Branch("nMuons",      nMuons,     Form("nMuons[%d]/I",pNPhysLayers) );
  hitTree->Branch("nElectrons",  nElectrons, Form("nElectrons[%d]/I",pNPhysLayers) );
  hitTree->Branch("genId",  &genId,   "genId/I");
  hitTree->Branch("genEn",  &genEn,   "genEn/F");
  hitTree->Branch("genEt",  &genEt,   "genEt/F");
  hitTree->Branch("genEta", &genEta,  "genEta/F");
  hitTree->Branch("genPhi", &genPhi,  "genPhi/F");
  hitTree->Branch("nhits",  &nhits,   "nhits/I");
  hitTree->Branch("hit_sithick",  hit_sithick,  "hit_sithick[nhits]/I");
  hitTree->Branch("hit_si",  hit_si,  "hit_si[nhits]/O");
  hitTree->Branch("hit_lay", hit_lay, "hit_lay[nhits]/I");
  hitTree->Branch("hit_x",   hit_x,   "hit_x[nhits]/F");
  hitTree->Branch("hit_y",   hit_y,   "hit_y[nhits]/F");
  hitTree->Branch("hit_z",   hit_z,   "hit_z[nhits]/F");
  hitTree->Branch("hit_men",  hit_men,  "hit_men[nhits]/F");
  hitTree->Branch("hit_en",  hit_en,  "hit_en[nhits]/F");
  hitTree->Branch("hit_endens",  hit_endens,  "hit_endens[nhits]/F");
  hitTree->Branch("hit_dR",  hit_dR,  "hit_dR[nhits]/F");
  hitTree->Branch("hit_dRho", hit_dRho, "hit_dRho[nhits]/F");
  hitTree->Branch("hit_miss", hit_miss, Form("hit_miss[%d]/F",pNPhysLayers) );
  hitTree->Branch("si_clustsumen", si_clustsumen, Form("si_clustsumen[5][%d]/F",pNPhysLayers) );
  hitTree->Branch("sci_clustsumen", sci_clustsumen, Form("sci_clustsumen[5][%d]/F",pNPhysLayers) );
  hitTree->Branch("si_sumen", si_sumen, Form("si_sumen[%d]/F",pNPhysLayers));
  hitTree->Branch("sci_sumen", sci_sumen, Form("sci_sumen[%d]/F",pNPhysLayers));
  hitTree->Branch("total_sim_men",  total_sim_men,  Form("total_sim_men[%d]/F",pNPhysLayers));
  hitTree->Branch("avg_emFrac",  avg_emFrac,  Form("avg_emFrac[%d]/F",pNPhysLayers));
  hitTree->Branch("avg_hadFrac",  avg_hadFrac,  Form("avg_hadFrac[%d]/F",pNPhysLayers));

  hitTree->SetAutoFlush(1000);
  hitTree->SetDirectory(outputFile);

  //section info (this just needs to be done for the first event)
  reco->GetEntry(0);
  std::vector<std::vector<float> > sectionInfo(nLayers);
  for(auto sec : *samplesecvec) 
    {
      int volNb(sec.volNb());
      std::vector<float> info(3,0);
      info[0]=sec.volX0trans();
      info[1]=sec.volLambdatrans();
      info[2]=sec.voldEdx();
      if(volNb<nLayers)
        sectionInfo[volNb]=info;
      else
        sectionInfo.push_back(info);
    }

  //fill histograms
  TH1F *x0H     = new TH1F("x0",";Section;X_{0}",      sectionInfo.size(),0,sectionInfo.size());
  TH1F *lambdaH = new TH1F("lambda",";Section;#lambda",sectionInfo.size(),0,sectionInfo.size());
  TH1F *dedxH   = new TH1F("dedx",";Section;dE/dx",    sectionInfo.size(),0,sectionInfo.size());
  for(size_t volNb=0; volNb<sectionInfo.size(); volNb++)
    {
      std::vector<float> &info=sectionInfo[volNb];
      if(info.size()<2) continue;
      x0H->Fill(volNb,info[0]);
      lambdaH->Fill(volNb,info[1]);
      dedxH->Fill(volNb,info[2]);
    }

  //de/dx weights
  std::vector<float> dedx_wgts;
  for(int i=0; i<(int)pNPhysLayers; i++){
    dedx_wgts.push_back( 0.5*( dedxH->GetBinContent(i+1)+dedxH->GetBinContent(i+2) ) );
  }

  //
  // LOOP OVER EVENTS
  //
  const unsigned nEvts = ((pNevts > reco->GetEntries() || pNevts==0) ? static_cast<unsigned>(reco->GetEntries()) : pNevts) ;
  for (unsigned ievt(0); ievt<nEvts; ++ievt) {

    reco->GetEntry(ievt);
    if (debug)             std::cout << "... Processing entry: " << ievt << std::endl;
    else if (ievt%50 == 0) std::cout << "... Processing entry: " << ievt << std::endl;

    //require one single generated particle hitting the detector
    if(genpartvec->size()!=1) continue;
    genId=(*genpartvec)[0].pdgid();
    genEn=1e-3*(*genpartvec)[0].E();
    genEt=1e-3*sqrt(pow((*genpartvec)[0].mass(),2)+pow((*genpartvec)[0].pt(),2));
    genEta=(*genpartvec)[0].eta();
    genPhi=(*genpartvec)[0].phi();
    Event = (*event).eventNumber(); 
    //reset energy sums and hit counts
    nhits=0;
    for(unsigned i=0; i<pNPhysLayers; i++) { 
      si_sumen[i]=0; 
      sci_sumen[i]=0;
      total_sim_men[i]=0;
      avg_emFrac[i]=0;
      avg_hadFrac[i]=0;
      hit_miss[i]=0;
      nHadrons[i]=0;
      nGammas[i]=0;
      nMuons[i]=0;
      nElectrons[i]=0;
      for(unsigned ir=0; ir<5; ir++) {
        si_clustsumen[ir][i]=0;
        sci_clustsumen[ir][i]=0;
      }
    }

    //sim-level information on the particles creating the hits
    for(auto hit : *simhitvec) {
      int ilay(hit.layer());
      if(ilay>(Int_t)pNPhysLayers) ilay-=nVirtualLayers; //scintillator layers are counted after Si and back plate in standalone
      total_sim_men[ilay]   += hit.energy();
      avg_emFrac[ilay]  += (hit.gFrac()+hit.eFrac())*hit.energy();
      avg_hadFrac[ilay] += (1-hit.gFrac()-hit.eFrac()-hit.muFrac())*hit.energy();

      nHadrons[ilay]+=hit.nNeutrons()+hit.nProtons()+hit.nHadrons();
      nGammas[ilay]+=hit.nGammas();
      nMuons[ilay]+=hit.nMuons();
      nElectrons[ilay]+=hit.nElectrons();
    }

    for(Int_t ilay=0; ilay<(Int_t)pNPhysLayers; ilay++) {
      if(total_sim_men[ilay]<=0) continue;
      avg_emFrac[ilay] /= total_sim_men[ilay];
      avg_hadFrac[ilay] /= total_sim_men[ilay];
    }

    //integrate rec hits and store more granular information on recHits around region of interest
    std::vector<PseudoJet> pseudoParticles;
    for (auto hit : *rechitvec ) {
      //apply some fiducial cuts
      float en(hit.energy());

      if(icsmear>0)
        en += gRandom->Gaus(0,icsmear*en);

      if(en<enmin)  continue;

      Int_t raw_lay(hit.layer());
      Int_t ilay(raw_lay+1);
      if(ilay>(Int_t)pNPhysLayers) ilay-=nVirtualLayers; //scintillator layers are counted after Si and back plate in standalone

      //save as pseudo-jet for the clustering
      PseudoJet ip=PseudoJet(hit.px(),hit.py(),hit.pz(),hit.energy());
      ip.set_user_index(ilay);
      pseudoParticles.push_back( ip );

      //select in a deltaR window
      float eta(hit.eta()),phi(hit.phi());
      float dR=sqrt( pow(eta-genEta,2)+pow(TVector2::Phi_mpi_pi(phi-genPhi),2) );
      bool miss(false);
      if(dR>dRwindow) miss=true;

      //cartesian distance
      float rhoCen = hit.get_z()/TMath::SinH(genEta);
      float xcen   = rhoCen*TMath::Cos(genPhi);
      float ycen   = rhoCen*TMath::Sin(genPhi);
      float dRho   = TMath::Sqrt( TMath::Power(hit.get_x()-xcen,2)+TMath::Power(hit.get_y()-ycen,2) );

      const HGCSSSubDetector & subdet = myDetector.subDetectorByLayer(raw_lay);
      float area(siArea*0.01);
      float wgt(dedx_wgts[ilay-1]);
      if(subdet.type>=BHCAL1) area=0.01*getSciArea(hit.get_z(),eta,phi,subdet.type==BHCAL1?sci1Side:sci2Side);
      si_sumen[ilay-1]  += en*wgt*(!subdet.isScint);
      sci_sumen[ilay-1] += en*wgt*(subdet.isScint);
      
      if(nhits>=MAXHITS) miss=true;
      miss = false;
      if(!miss)
        {
          hit_lay[nhits]    = ilay;
          hit_x[nhits]      = hit.get_x();
          hit_y[nhits]      = hit.get_y();        
          hit_z[nhits]      = hit.get_z();
          hit_men[nhits]    = en;
          hit_en[nhits]     = hit_men[nhits]*dedx_wgts[ilay-1];
          hit_endens[nhits] = hit_en[nhits]/area;
          hit_dR[nhits]     = dR;
          hit_dRho[nhits]   = dRho;
          hit_si[nhits]     = !subdet.isScint;
          float radius(sqrt(pow(hit.get_x(),2)+pow(hit.get_y(),2)));
          hit_sithick[nhits] = geomConv.getNumberOfSiLayers(subdet.type,radius,hit.get_z());
          nhits++;
        }
      else
          hit_miss[ilay] += en*dedx_wgts[ilay-1];
    }
    
    //run fast jet on rec hits and save energy clustered in each layer
    float R[5]={0.1,0.2,0.3,0.5};
    for(size_t ir=0; ir<5; ir++)
      {     
        JetDefinition jet_def(antikt_algorithm, R[ir]);
        ClusterSequence cs(pseudoParticles, jet_def);
        std::vector<PseudoJet> jets = sorted_by_pt(cs.inclusive_jets());
        
        //get closest in direction
        float minDR(9999);
        int jidx(-1);
        for(size_t j=0; j<jets.size(); j++)
          {
            float dR=sqrt( pow(jets[j].eta()-genEta,2)+pow(TVector2::Phi_mpi_pi(jets[j].phi()-genPhi),2));
            if(dR>minDR) continue;
            minDR=dR;
            jidx=j;
          }

        if(jidx>=0)
          {
            for(auto jconst :  jets[jidx].constituents())
              {
                int ilay=jconst.user_index()-1;
                const HGCSSSubDetector & subdet = myDetector.subDetectorByLayer(ilay);
                float wgt(dedx_wgts[ilay]);
                si_clustsumen[ir][ilay]  += jconst.E()*wgt*(!subdet.isScint);
                sci_clustsumen[ir][ilay] += jconst.E()*wgt*(subdet.isScint);
              }
          }
      }

    if(nhits==0) continue;
    hitTree->Fill();
  }

  //write results to file  
  outputFile->cd();
  dedxH->Write();
  x0H->Write();
  lambdaH->Write();
  hitTree->Write();
  outputFile->Close();

  //copy to final output
  gSystem->Exec( "mv -v " + baseOutputName + " " + outFile );

  return 0; 
}
