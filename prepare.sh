#!/bin/bash 

echo $PWD
cd $PWD

cd /afs/cern.ch/user/k/kalpana/work/public/hgcalTestBeamOct2018/Hgcal_fullSim_phase2/StandAlonHGCALSim/CMSSW_12_0_0/src/Latest_try/PFCal/HGCStandaloneAnalysis
echo $PWD
# source /afs/cern.ch/user/k/kalpana/work/public/hgcalTestBeamOct2018/Hgcal_fullSim_phase2/StandAlonHGCALSim/CMSSW_12_0_0/src/Latest_try/PFCal/PFCalEE/g4env.sh
# cd /afs/cern.ch/user/k/kalpana/work/public/hgcalTestBeamOct2018/Hgcal_fullSim_phase2/StandAlonHGCALSim/CMSSW_12_0_0/src/Latest_try/PFCal/PFCalEE/
# source g4env.sh
# mkdir -p userlib/{lib,obj,bin} && cd userlib && make dictionary && make -j 5 && cd - && ./makeG4
# cd /afs/cern.ch/user/k/kalpana/work/public/hgcalTestBeamOct2018/Hgcal_fullSim_phase2/StandAlonHGCALSim/CMSSW_12_0_0/src/Latest_try/PFCal/HGCStandaloneAnalysis
# source /afs/cern.ch/user/k/kalpana/work/public/hgcalTestBeamOct2018/Hgcal_fullSim_phase2/StandAlonHGCALSim/CMSSW_12_0_0/src/Latest_try/PFCal/PFCalEE/g4env.sh
export PYTHONDIR=/afs/cern.ch/sw/lcg/external/Python/2.7.3/$ARCH/
export PYTHONPATH=$PYTHONPATH:$ROOTSYS/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib:$PYTHONDIR/lib:`pwd`/lib
#make
echo $1
./bin/prepareCalibrationTree --cfg $1
