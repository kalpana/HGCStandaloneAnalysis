#!/bin/bash

outDir=/eos/cms/store/cmst3/user/psilva/HGCal/scenario13/2021Apr08/
mkdir -p ${outDir};

### PIONS
particle=pi-
indir=/eos/cms/store/cmst3/group/hgcal/Geant4/gitV08-08-00-v3/
#for icsmear in -1 0.1 0.2 0.3 0.5; do
for icsmear in 0.1 0.2; do

    version=69
    python test/submitToFarm.py \
        -e prepareCalibrationTree \
        --out ${outDir}/Geant4_${particle}_version${version}.root \
        --sim HGcal_version${version}_model2_BON \
        --reco Digi_200uversion${version}_model2_BON_npuvtx0_ic3 \
        --physLayers 52 \
        --icsmear ${icsmear} \
        -n 25 \
        --jobs 8 \
        ${indir}/${particle}/

    version=73
    python test/submitToFarm.py \
        -e prepareCalibrationTree \
        --out ${outDir}/Geant4_${particle}_version${version}.root \
        --sim HGcal_version${version}_model2_BON \
        --reco Digi_200uversion${version}_model2_BON_npuvtx0_ic3 \
        --physLayers 47 \
        --icsmear ${icsmear} \
        -n 25 \
        --jobs 12 \
        ${indir}/${particle}/
done

exit -1

#### PHOTONS
particle=gamma
version=60
indir=/eos/cms/store/group/dpg_hgcal/comm_hgcal/bfontana/gitV08-08-00_20K
python test/submitToFarm.py \
    -e prepareCalibrationTree \
    --out ${outDir}/Geant4_${particle}_version${version}.root \
    --sim HGcal_version${version}_model2_BON \
    --reco Digi_200uversion${version}_model2_BON_npuvtx0_ic3 \
    --physLayers 28 \
    -n 30 \
    --jobs 12 \
    ${indir}/${particle}/    

version=70
python test/submitToFarm.py \
    -e prepareCalibrationTree \
    --out ${outDir}/Geant4_${particle}_version${version}.root \
    --sim HGcal_version${version}_model2_BON \
    --reco Digi_200uversion${version}_model2_BON_npuvtx0_ic3 \
    --physLayers 26 \
    -n 30 \
    --jobs 12 \
    ${indir}/${particle}/

python test/checkNtupleIntegrity.py ${outDir}
