outDir=/eos/cms/store/group/dpg_hgcal/comm_hgcal/kalpana/gitV08-08-00-v3/pi-/Skimmed_Files/
version=73
particle=pi-
#mkdir -p ${outDir};

python test/submitToFarm.py \
       -e prepareCalibrationTree \
       --out ${outDir}/Geant4_${particle}_version${version}.root \
       --sim HGcal_version73_model2_BOFF \
       --reco Digi_200uversion73_model2_BOFF_npuvtx0_ic3 \
       --physLayers 47 \
       -n 100 \
    --submit\
       /eos/cms/store/group/dpg_hgcal/comm_hgcal/kalpana/gitV08-08-00-v3/${particle}/
